public class TetrisProtocol {
	public static final String DUMP = "dump";

	public static final String PURGED = "purged";

	public static final String ENDGAME = "gameOver";

	public static final String VICTORY = "victory";
}
