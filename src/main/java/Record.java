public class Record {
	// private String label;

	// private int value;

	public static final int FIELDS = 9;

	private int score;

	private int time;

	private int level;

	private int lines;

	private int singles;

	private int doubles;

	private int triples;

	private int tetrises;

	private String initials;

	public String toString() {
		return score + " " + time + " " + level + " " + lines + " " + singles
				+ " " + doubles + " " + triples + " " + tetrises + " "
				+ initials;
	}

	public Record(int score, int time, int level, int lines, int singles,
			int doubles, int triples, int tetrises, String initials) {
		super();
		this.doubles = doubles;
		this.initials = initials;
		this.level = level;
		this.lines = lines;
		this.score = score;
		this.singles = singles;
		this.tetrises = tetrises;
		this.time = time;
		this.triples = triples;
	}

	/**
	 * @return Returns the initials.
	 */
	public String getInitials() {
		return initials;
	}

	/**
	 * @param initials
	 *            The initials to set.
	 */
	public void setInitials(String initials) {
		this.initials = initials;
	}

	/**
	 * @return Returns the doubles.
	 */
	public int getDoubles() {
		return doubles;
	}

	/**
	 * @param doubles
	 *            The doubles to set.
	 */
	public void setDoubles(int doubles) {
		this.doubles = doubles;
	}

	/**
	 * @return Returns the lines.
	 */
	public int getLines() {
		return lines;
	}

	/**
	 * @param lines
	 *            The lines to set.
	 */
	public void setLines(int lines) {
		this.lines = lines;
	}

	/**
	 * @return Returns the score.
	 */
	public int getScore() {
		return score;
	}

	/**
	 * @param score
	 *            The score to set.
	 */
	public void setScore(int score) {
		this.score = score;
	}

	/**
	 * @return Returns the singles.
	 */
	public int getSingles() {
		return singles;
	}

	/**
	 * @param singles
	 *            The singles to set.
	 */
	public void setSingles(int singles) {
		this.singles = singles;
	}

	/**
	 * @return Returns the tetrises.
	 */
	public int getTetrises() {
		return tetrises;
	}

	/**
	 * @param tetrises
	 *            The tetrises to set.
	 */
	public void setTetrises(int tetrises) {
		this.tetrises = tetrises;
	}

	/**
	 * @return Returns the time.
	 */
	public int getTime() {
		return time;
	}

	/**
	 * @param time
	 *            The time to set.
	 */
	public void setTime(int time) {
		this.time = time;
	}

	/**
	 * @return Returns the triples.
	 */
	public int getTriples() {
		return triples;
	}

	/**
	 * @param triples
	 *            The triples to set.
	 */
	public void setTriples(int triples) {
		this.triples = triples;
	}

	/**
	 * @return Returns the level.
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * @param level
	 *            The level to set.
	 */
	public void setLevel(int level) {
		this.level = level;
	}

}
