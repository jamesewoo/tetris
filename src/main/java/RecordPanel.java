import java.io.*;
import java.util.StringTokenizer;
import javax.swing.*;

import java.awt.*;

public class RecordPanel extends JPanel {
	public static final int PANEL_WIDTH = 580;

	public static final int PANEL_HEIGHT = 150;

	public static final int RECORDS = 8;

	public static final String DEFAULT_RECORDFILE = "src/test/resources/records";

	public String recordFile = DEFAULT_RECORDFILE;

	private Record highScore;

	private Record highTime;

	private Record highLevel;

	private Record highLines;

	private Record highSingles;

	private Record highDoubles;

	private Record highTriples;

	private Record highTetrises;

	private JTable table;

	private JScrollPane scrollPane = new JScrollPane();

	public RecordPanel(String filename) {
		super();
		setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));
		this.recordFile = filename;

		load();
		add(scrollPane);
		setTable();

		table.setPreferredScrollableViewportSize(new Dimension(500, 100));

		// repaint();
	}

	public RecordPanel() {
		this(DEFAULT_RECORDFILE);
	}

	public void load() {
		try {
			String line;
			int score;
			int time;
			int level;
			int lines;
			int singles;
			int doubles;
			int triples;
			int tetrises;
			String initials;
			BufferedReader inStream = new BufferedReader(new FileReader(
					recordFile));
			// while ((line = inStream.readLine()) != null) {
			for (int i = 0; i < RecordPanel.RECORDS; ++i) {
				line = inStream.readLine();
				StringTokenizer tokens = new StringTokenizer(line);
				score = Integer.parseInt(tokens.nextToken());
				time = Integer.parseInt(tokens.nextToken());
				level = Integer.parseInt(tokens.nextToken());
				lines = Integer.parseInt(tokens.nextToken());
				singles = Integer.parseInt(tokens.nextToken());
				doubles = Integer.parseInt(tokens.nextToken());
				triples = Integer.parseInt(tokens.nextToken());
				tetrises = Integer.parseInt(tokens.nextToken());
				initials = tokens.nextToken();
				switch (i) {
				case 0:
					highScore = new Record(score, time, level, lines, singles,
							doubles, triples, tetrises, initials);
					break;
				case 1:
					highTime = new Record(score, time, level, lines, singles,
							doubles, triples, tetrises, initials);
					break;
				case 2:
					highLevel = new Record(score, time, level, lines, singles,
							doubles, triples, tetrises, initials);
				case 3:
					highLines = new Record(score, time, level, lines, singles,
							doubles, triples, tetrises, initials);
					break;
				case 4:
					highSingles = new Record(score, time, level, lines,
							singles, doubles, triples, tetrises, initials);
					break;
				case 5:
					highDoubles = new Record(score, time, level, lines,
							singles, doubles, triples, tetrises, initials);
					break;
				case 6:
					highTriples = new Record(score, time, level, lines,
							singles, doubles, triples, tetrises, initials);
					break;
				case 7:
					highTetrises = new Record(score, time, level, lines,
							singles, doubles, triples, tetrises, initials);
					break;
				default:
					break;
				}
			}
			inStream.close();
		} catch (IOException e) {
			// do nothing
		}
	}

	public void setTable() {
		String[] columnNames = { "Score", "Time", "Level", "Lines", "Single",
				"Double", "Triple", "Tetris", "Initials" };

		Object[][] data = {
				{ new Integer(highScore.getScore()),
						new Integer(highScore.getTime()),
						new Integer(highScore.getLevel()),
						new Integer(highScore.getLines()),
						new Integer(highScore.getSingles()),
						new Integer(highScore.getDoubles()),
						new Integer(highScore.getTriples()),
						new Integer(highScore.getTetrises()),
						highScore.getInitials() },
				{ new Integer(highTime.getScore()),
						new Integer(highTime.getTime()),
						new Integer(highTime.getLevel()),
						new Integer(highTime.getLines()),
						new Integer(highTime.getSingles()),
						new Integer(highTime.getDoubles()),
						new Integer(highTime.getTriples()),
						new Integer(highTime.getTetrises()),
						highTime.getInitials() },
				{ new Integer(highLevel.getScore()),
						new Integer(highLevel.getTime()),
						new Integer(highLevel.getLevel()),
						new Integer(highLevel.getLines()),
						new Integer(highLevel.getSingles()),
						new Integer(highLevel.getDoubles()),
						new Integer(highLevel.getTriples()),
						new Integer(highLevel.getTetrises()),
						highLevel.getInitials() },
				{ new Integer(highLines.getScore()),
						new Integer(highLines.getTime()),
						new Integer(highScore.getLevel()),
						new Integer(highLines.getLines()),
						new Integer(highLines.getSingles()),
						new Integer(highLines.getDoubles()),
						new Integer(highLines.getTriples()),
						new Integer(highLines.getTetrises()),
						highLines.getInitials() },
				{ new Integer(highSingles.getScore()),
						new Integer(highSingles.getTime()),
						new Integer(highSingles.getLevel()),
						new Integer(highSingles.getLines()),
						new Integer(highSingles.getSingles()),
						new Integer(highSingles.getDoubles()),
						new Integer(highSingles.getTriples()),
						new Integer(highSingles.getTetrises()),
						highSingles.getInitials() },
				{ new Integer(highDoubles.getScore()),
						new Integer(highDoubles.getTime()),
						new Integer(highDoubles.getLevel()),
						new Integer(highDoubles.getLines()),
						new Integer(highDoubles.getSingles()),
						new Integer(highDoubles.getDoubles()),
						new Integer(highDoubles.getTriples()),
						new Integer(highDoubles.getTetrises()),
						highDoubles.getInitials() },
				{ new Integer(highTriples.getScore()),
						new Integer(highTriples.getTime()),
						new Integer(highTriples.getLevel()),
						new Integer(highTriples.getLines()),
						new Integer(highTriples.getSingles()),
						new Integer(highTriples.getDoubles()),
						new Integer(highTriples.getTriples()),
						new Integer(highTriples.getTetrises()),
						highTriples.getInitials() },
				{ new Integer(highTetrises.getScore()),
						new Integer(highTetrises.getTime()),
						new Integer(highTetrises.getLevel()),
						new Integer(highTetrises.getLines()),
						new Integer(highTetrises.getSingles()),
						new Integer(highTetrises.getDoubles()),
						new Integer(highTetrises.getTriples()),
						new Integer(highTetrises.getTetrises()),
						highTetrises.getInitials() } };

		table = new JTable(data, columnNames);
		this.remove(scrollPane); // TODO careful
		scrollPane = new JScrollPane(table);
		this.add(scrollPane);
	}

	public void save() {
		try {
			BufferedWriter outStream = new BufferedWriter(new FileWriter(
					recordFile));
			outStream.write(highScore.toString());
			outStream.newLine();
			outStream.write(highTime.toString());
			outStream.newLine();
			outStream.write(highLevel.toString());
			outStream.newLine();
			outStream.write(highLines.toString());
			outStream.newLine();
			outStream.write(highSingles.toString());
			outStream.newLine();
			outStream.write(highDoubles.toString());
			outStream.newLine();
			outStream.write(highTriples.toString());
			outStream.newLine();
			outStream.write(highTetrises.toString());
			outStream.newLine();
			outStream.close();
		} catch (IOException e) {
			// do nothing
		}
	}

	/**
	 * @return Returns the pANEL_HEIGHT.
	 */
	public static int getPANEL_HEIGHT() {
		return PANEL_HEIGHT;
	}

	/**
	 * @return Returns the pANEL_WIDTH.
	 */
	public static int getPANEL_WIDTH() {
		return PANEL_WIDTH;
	}

	/**
	 * @return Returns the highScore.
	 */
	public Record getHighScore() {
		return highScore;
	}

	/**
	 * @param highScore
	 *            The highScore to set.
	 */
	public void setHighScore(Record highScore) {
		this.highScore = highScore;
	}

	/**
	 * @return Returns the highDoubles.
	 */
	public Record getHighDoubles() {
		return highDoubles;
	}

	/**
	 * @param highDoubles
	 *            The highDoubles to set.
	 */
	public void setHighDoubles(Record highDoubles) {
		this.highDoubles = highDoubles;
	}

	/**
	 * @return Returns the highLines.
	 */
	public Record getHighLines() {
		return highLines;
	}

	/**
	 * @param highLines
	 *            The highLines to set.
	 */
	public void setHighLines(Record highLines) {
		this.highLines = highLines;
	}

	/**
	 * @return Returns the highSingles.
	 */
	public Record getHighSingles() {
		return highSingles;
	}

	/**
	 * @param highSingles
	 *            The highSingles to set.
	 */
	public void setHighSingles(Record highSingles) {
		this.highSingles = highSingles;
	}

	/**
	 * @return Returns the highTetrises.
	 */
	public Record getHighTetrises() {
		return highTetrises;
	}

	/**
	 * @param highTetrises
	 *            The highTetrises to set.
	 */
	public void setHighTetrises(Record highTetrises) {
		this.highTetrises = highTetrises;
	}

	/**
	 * @return Returns the highTime.
	 */
	public Record getHighTime() {
		return highTime;
	}

	/**
	 * @param highTime
	 *            The highTime to set.
	 */
	public void setHighTime(Record highTime) {
		this.highTime = highTime;
	}

	/**
	 * @return Returns the highTriples.
	 */
	public Record getHighTriples() {
		return highTriples;
	}

	/**
	 * @param highTriples
	 *            The highTriples to set.
	 */
	public void setHighTriples(Record highTriples) {
		this.highTriples = highTriples;
	}

	/**
	 * @return Returns the recordFile.
	 */
	public String getRecordFile() {
		return recordFile;
	}

	/**
	 * @param recordFile
	 *            The recordFile to set.
	 */
	public void setRecordFile(String recordFile) {
		this.recordFile = recordFile;
	}

	/**
	 * @return Returns the highLevel.
	 */
	public Record getHighLevel() {
		return highLevel;
	}

	/**
	 * @param highLevel
	 *            The highLevel to set.
	 */
	public void setHighLevel(Record highLevel) {
		this.highLevel = highLevel;
	}
}
