import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SplashPanel extends JPanel implements ActionListener {
	private static final int PANEL_WIDTH = 200;

	private static final int PANEL_HEIGHT = 100;

	private JFrame container;

	private JButton single;

	private JButton multi;

	private JButton host;

	private JButton join;

	private String singleLabel = "Single Player";

	private String multiLabel = "Multiplayer";

	private String hostLabel = "Host Game";

	private String joinLabel = "Join Game";

	public SplashPanel(JFrame container) {
		super();
		this.container = container;
		setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));

		single = new JButton(singleLabel);
		host = new JButton(hostLabel);
		join = new JButton(joinLabel);

		single.addActionListener(this);
		host.addActionListener(this);
		join.addActionListener(this);

		this.add(single);
		this.add(host);
		this.add(join);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == single) {
			container.dispose();
			TetrisApp.startSingle();
		} else if (e.getSource() == host) {
			// container.dispose();
			TetrisApp.startHost();
		} else if (e.getSource() == join) {
			// container.dispose();
			TetrisApp.startJoin();
		}
	}
}
