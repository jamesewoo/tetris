import javax.swing.*;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class HelpPanel extends JPanel implements KeyListener {
	private static final int PANEL_WIDTH = 450;

	private static final int PANEL_HEIGHT = 250;

	private TetrisPanel parent;

	private JTable table;

	private JScrollPane scrollPane;

	public HelpPanel(TetrisPanel parent) {
		super();
		parent = (TetrisPanel) getParent();
		setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));

		String[] columnNames = { "Action", "Default Key", "Alternate Key" };

		Object[][] data = { { "Move Left", "Left Arrow", "" },
				{ "Move Right", "Right Arrow", "" },
				{ "Move Down", "Down Arrow", "" },
				{ "Rotate", "Up Arrow", "Shift" },
				{ "Instant Drop", "Shift", "Z" }, { "Pause", "P", "" },
				{ "New Game", "N", "" }, { "Records", "R", "" },
				{ "Options", "O", "" }, };

		table = new JTable(data, columnNames);
		table.setPreferredScrollableViewportSize(new Dimension(400, 200));
		scrollPane = new JScrollPane(table);
		this.add(scrollPane);
		// repaint();
	}

	/*
	 * public void paint(Graphics g) { g.clearRect(0, 0, PANEL_WIDTH,
	 * PANEL_HEIGHT); g.drawString("Move Left............. Left Arrow", 10, 15);
	 * g.drawString("Move Right............ Right Arrow", 10, 30);
	 * g.drawString("Move Down............. Down Arrow", 10, 45);
	 * g.drawString("Rotate................ Up Arrow", 10, 60);
	 * g.drawString("Instant Drop.......... Shift", 10, 75);
	 * g.drawString("Rotate................ Shift", 10, 60);
	 * g.drawString("Instant Drop.......... Z", 10, 75);
	 * g.drawString("Pause................. P", 10, 90); g.drawString("New
	 * Game.............. N", 10, 105); g.drawString("Records............... R",
	 * 10, 120); g.drawString("Options............... O", 10, 135); }
	 */

	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		if (key == TetrisPanel.HELP_BUTTON) {
			// TODO close window
			// getParent().setVisible(false);
		}
	}

	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}
}
