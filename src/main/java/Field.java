public class Field {
	private int rows;

	private int cols;

	private Space[][] spaceArr;

	public Field(int rows, int cols) {
		super();
		this.cols = cols;
		this.rows = rows;
		this.spaceArr = new Space[rows][cols];
		for (int i = 0; i < rows; ++i) {
			for (int j = 0; j < cols; ++j) {
				this.spaceArr[i][j] = new Space(i, j);
			}
		}
	}

	/**
	 * @return Returns the cols.
	 */
	public int getCols() {
		return cols;
	}

	/**
	 * @param cols
	 *            The cols to set.
	 */
	public void setCols(int cols) {
		this.cols = cols;
	}

	/**
	 * @return Returns the rows.
	 */
	public int getRows() {
		return rows;
	}

	/**
	 * @param rows
	 *            The rows to set.
	 */
	public void setRows(int rows) {
		this.rows = rows;
	}

	/**
	 * @return Returns the spaceArr.
	 */
	public Space[][] getSpaceArr() {
		return spaceArr;
	}

	/**
	 * @param spaceArr
	 *            The spaceArr to set.
	 */
	public void setSpaceArr(Space[][] spaceArr) {
		this.spaceArr = spaceArr;
	}

	public Space getSpaceAt(int rownum, int colnum) {
		return spaceArr[rownum - 1][colnum - 1];
	}
}
