import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.StringTokenizer;

import javax.swing.JOptionPane;

public class TetrisClient extends TetrisPanel {

	private TetrisWatcher watcher;

	private static final int PORT = 4444;

	public static final String DEFAULT_HOST = "localhost";

	private String host;

	private int playerID;

	private Socket sock;

	private BufferedReader in;

	private PrintWriter out;

	private final double DUMP_CONSTANT = .1;

	public TetrisClient() {
		this(DEFAULT_HOST);
	}

	public TetrisClient(String host) {
		super();
		this.host = host;
		if (host == null || host.length() < 1) {
			host = DEFAULT_HOST;
		}
		connect();
	}

	private void connect() {
		try {
			sock = new Socket(host, PORT);
			in = new BufferedReader(
					new InputStreamReader(sock.getInputStream()));
			out = new PrintWriter(sock.getOutputStream(), true);

			// get ID
			StringTokenizer tokens = new StringTokenizer(in.readLine());
			tokens.nextToken();
			playerID = Integer.parseInt(tokens.nextToken());

			watcher = new TetrisWatcher(this, in);
			watcher.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void send(String msg) {
		send(msg, true);
	}

	private void send(String msg, boolean echo) {
		if (echo) {
			System.out.println("Player " + playerID + ": " + msg);
		}
		try {
			out.println(msg);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// TODO
	public void dumpRandom(int magnitude) {
		// obtain top-level block
		int topLevel = 0;
		for (int i = 0; i < theField.getRows(); ++i) {
			for (int j = 0; j < theField.getCols(); ++j) {
				if (!theField.getSpaceArr()[i][j].isOccupied()) {
					topLevel = i;
					break;
				}
			}
		}
		// fill above
		for (int s = topLevel - 1; s >= topLevel - magnitude; --s) {
			if (s < 0) {
				break;
			}
			for (int t = 0; t < theField.getCols(); ++t) {
				if (!theField.getSpaceArr()[s][t].isOccupied()
						&& Math.pow(Math.random(), magnitude) < DUMP_CONSTANT) {
					theField.getSpaceArr()[s][t].setOccupied(true);
				}
			}
		}
		repaint();
	}

	// TODO
	public void purgeLine() {
		super.purgeLine();
		if (linesInPurge > 0) {
			send(TetrisProtocol.PURGED + " " + linesInPurge, true);
		}
	}

	public void endGame() {
		super.endGame();
		send(TetrisProtocol.ENDGAME, true);
	}

	// TODO
	public void victory() {
		// System.out.println("Player " + playerID + " wins");
		// System.exit(0);
	}

	/**
	 * @return Returns the playerID.
	 */
	public int getPlayerID() {
		return playerID;
	}

	/**
	 * @param playerID
	 *            The playerID to set.
	 */
	public void setPlayerID(int playerID) {
		this.playerID = playerID;
	}
}
