import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;

public class OptionsPanel extends JPanel implements ItemListener {
	private static final int PANEL_WIDTH = 300;

	private static final int PANEL_HEIGHT = 70;

	private JComboBox tilesetMenu;

	private JComboBox musicMenu;

	private JCheckBox soundBox;

	private JCheckBox alternateKeys;

	// private JCheckBox musicBox;

	private String soundBoxLabel = "Play Sounds";

	private String alternateKeysLabel = "Use Alternate Keys";

	private TetrisPanel parent;

	private String noMusicLabel = "No Music";

	private String randomLabel = "Random";

	private final String[] tilesets = { "Black Tiles", "Blue Tiles" };

	private final String[] songs = { noMusicLabel, randomLabel, "Human1gm",
			"Human2gm", "Human3gm", "Human4gm", "Orc1gm", "Orc2gm", "Orc3gm",
			"Orc4gm", /* "Humdthgm" */};

	public OptionsPanel(TetrisPanel parent) {
		super();
		this.parent = parent;
		setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));
		tilesetMenu = new JComboBox();
		musicMenu = new JComboBox();
		soundBox = new JCheckBox(soundBoxLabel, parent.getSoundIsOn());
		if (parent.getSoundIsOn()) {
			parent.getSoundManager().preloadAll();
		}
		alternateKeys = new JCheckBox(alternateKeysLabel, parent
				.getAlternateKeys());
		// musicBox = new JCheckBox(musicBoxLabel, parent.getMusicIsOn());
		// tilesetMenu.setBounds(0, 0, 200, 20);
		this.add(tilesetMenu);
		this.add(musicMenu);
		this.add(soundBox);
		this.add(alternateKeys);
		// this.add(musicBox);
		for (int i = 0; i < tilesets.length; ++i) {
			tilesetMenu.addItem(tilesets[i]);
		}
		for (int i = 0; i < songs.length; ++i) {
			musicMenu.addItem(songs[i]);
		}
		tilesetMenu.setEditable(false);
		tilesetMenu.addItemListener(this);
		musicMenu.setEditable(false);
		musicMenu.addItemListener(this);
		soundBox.addItemListener(this);
		alternateKeys.addItemListener(this);
		// repaint();
	}

	/*
	 * public void paint(Graphics g) { g.clearRect(0, 0, PANEL_WIDTH,
	 * PANEL_HEIGHT); // g.drawString("Tileset", PANEL_WIDTH / 2 - 140, 50); //
	 * g.drawString("Music", PANEL_WIDTH / 2 - 35, 50); tilesetMenu.repaint();
	 * musicMenu.repaint(); soundBox.repaint(); // musicBox.repaint(); }
	 */

	public String getRandomSong() {
		return "src/main/resources/music/"
				+ songs[(int) Math.floor((songs.length - 2) * Math.random()) + 2]
				+ ".mid";
	}

	public void itemStateChanged(ItemEvent event) {
		if (event.getSource() == tilesetMenu
				&& event.getStateChange() == ItemEvent.SELECTED) {
			if (tilesetMenu.getSelectedItem() == tilesets[0]) {
				parent.setBlockCell(Toolkit.getDefaultToolkit().getImage(
						"src/main/resources/images/block2.jpg"));
			} else if (tilesetMenu.getSelectedItem() == tilesets[1]) {
				parent.setBlockCell(Toolkit.getDefaultToolkit().getImage(
						"src/main/resources/images/block5f.jpg"));
			}
		}
		if (event.getSource() == musicMenu
				&& event.getStateChange() == ItemEvent.SELECTED) {
			if (musicMenu.getSelectedItem() == noMusicLabel) {
				parent.setMusicIsOn(false);
				parent.stopMusic();
			} else if (musicMenu.getSelectedItem() == randomLabel) {
				parent.setMusicIsOn(true);
				parent.setMusicFile(getRandomSong());
				parent.restartMusic();
			} else {
				parent.setMusicIsOn(true);
				parent.setMusicFile("src/main/resources/music/" + musicMenu.getSelectedItem()
						+ ".mid");
				// musicBox.setState(false);
				parent.restartMusic();
			}
		}
		if (event.getSource() == soundBox
				&& event.getStateChange() == ItemEvent.SELECTED) {
			parent.setSoundIsOn(true);
			parent.getSoundManager().preloadAll();
		} else if (event.getSource() == soundBox
				&& event.getStateChange() == ItemEvent.DESELECTED) {
			parent.setSoundIsOn(false);
		}
		if (event.getSource() == alternateKeys
				&& event.getStateChange() == ItemEvent.SELECTED) {
			parent.setAlternateKeys(true);
		} else if (event.getSource() == alternateKeys
				&& event.getStateChange() == ItemEvent.DESELECTED) {
			parent.setAlternateKeys(false);
		}
		/*
		 * if (event.getSource() == musicBox && event.getStateChange() ==
		 * ItemEvent.SELECTED) { if (musicBox.getSelectedObjects()[0] ==
		 * musicBoxLabel) { parent.setMusicIsOn(true); parent.playMusic(); }
		 * else if (musicBox.getSelectedObjects()[0] == null) {
		 * parent.setMusicIsOn(false); parent.stopMusic(); } }
		 */
	}

	/**
	 * @return Returns the musicMenu.
	 */
	public JComboBox getMusicMenu() {
		return musicMenu;
	}

	/**
	 * @param musicMenu
	 *            The musicMenu to set.
	 */
	public void setMusicMenu(JComboBox musicMenu) {
		this.musicMenu = musicMenu;
	}

	/**
	 * @return Returns the noMusicLabel.
	 */
	public String getNoMusicLabel() {
		return noMusicLabel;
	}

	/**
	 * @param noMusicLabel
	 *            The noMusicLabel to set.
	 */
	public void setNoMusicLabel(String noMusicLabel) {
		this.noMusicLabel = noMusicLabel;
	}

	/**
	 * @return Returns the randomLabel.
	 */
	public String getRandomLabel() {
		return randomLabel;
	}

	/**
	 * @param randomLabel
	 *            The randomLabel to set.
	 */
	public void setRandomLabel(String randomLabel) {
		this.randomLabel = randomLabel;
	}
}