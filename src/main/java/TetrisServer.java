import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.StringTokenizer;

public class TetrisServer extends Thread {
	private static final int PORT = 4444;

	private static final int MAX_PLAYERS = 2;

	private ServerSocket serverSock;

	private Socket[] clientSock = new Socket[MAX_PLAYERS];

	private BufferedReader[] in = new BufferedReader[MAX_PLAYERS];

	private PrintWriter[] out = new PrintWriter[MAX_PLAYERS];

	private int players;

	public TetrisServer() {
		players = 0;
		this.setPriority(MAX_PRIORITY);
		start();
	}

	public void run() {
		try {
			serverSock = new ServerSocket(PORT);
			String line = null;

			while (players < MAX_PLAYERS) {
				System.out.println("Waiting for a client...");
				clientSock[players] = serverSock.accept();
				addPlayer(clientSock[players]);
			}

			// TODO
			// int i = 0;
			while (true) {
				// for (int i = 0; i < MAX_PLAYERS; ++i) {
				int i = (int) Math.floor(MAX_PLAYERS * Math.random());
				if ((line = in[i].readLine()) != null) {
					processInput(i + 1, line);
				}

				/*
				 * if (++i >= MAX_PLAYERS) { i = 0; }
				 */
				// }
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private synchronized void processInput(int playerID, String line) {
		// System.out.println(line);
		// if (line.lastIndexOf(TetrisProtocol.PURGED) != -1) {
		if (line.startsWith(TetrisProtocol.PURGED)) {
			int toID = playerID + 1;
			if (toID > MAX_PLAYERS) {
				toID = 1;
			}
			// get linesInPurge
			StringTokenizer tokens = new StringTokenizer(line);
			tokens.nextToken();
			send(toID, TetrisProtocol.DUMP + " "
					+ Integer.parseInt(tokens.nextToken()), true);
		} // else if (line.lastIndexOf(TetrisProtocol.ENDGAME) != -1) {
		else if (line.startsWith(TetrisProtocol.ENDGAME)) {
			int toID = playerID - 1;
			if (toID < 1) {
				toID = MAX_PLAYERS;
			}
			send(toID, TetrisProtocol.VICTORY, true);
		}
	}

	public synchronized void addPlayer(Socket clientSock) {
		try {
			// if (clientSock.isConnected()) {
			in[players] = new BufferedReader(new InputStreamReader(clientSock
					.getInputStream()));
			out[players] = new PrintWriter(clientSock.getOutputStream(), true);
			++players;

			sendAll("Player " + players + " has joined the game.", true);
			// }
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public synchronized void removePlayer(int playerID) {
	}

	private synchronized void send(int playerID, String msg) {
		send(playerID, msg, true);
	}

	private synchronized void send(int playerID, String msg, boolean echo) {
		if (echo) {
			System.out.println("To Player " + playerID + ": " + msg);
		}
		try {
			out[playerID - 1].println(msg);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	private synchronized void sendAll(String msg) {
		sendAll(msg, true);
	}

	private synchronized void sendAll(String msg, boolean echo) {
		if (echo) {
			System.out.println(msg);
		}
		try {
			for (int i = 0; i < players; ++i) {
				out[i].println(msg);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
