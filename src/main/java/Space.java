public class Space {
	private static final int HEIGHT = 20;

	private static final int WIDTH = 20;

	// start counting at 1
	private int rownum;

	// start counting at 1
	private int colnum;

	private boolean isOccupied;

	public Space(Space s) {
		this.colnum = s.colnum;
		this.rownum = s.rownum;
		this.isOccupied = s.isOccupied;
	}

	public Space(int rownum, int colnum) {
		super();
		this.colnum = colnum;
		this.rownum = rownum;
		this.isOccupied = false;
	}

	/**
	 * @return Returns the colnum.
	 */
	public int getColnum() {
		return colnum;
	}

	/**
	 * @param colnum
	 *            The colnum to set.
	 */
	public void setColnum(int colnum) {
		this.colnum = colnum;
	}

	/**
	 * @return Returns the isOccupied.
	 */
	public boolean isOccupied() {
		return isOccupied;
	}

	/**
	 * @param isOccupied
	 *            The isOccupied to set.
	 */
	public void setOccupied(boolean isOccupied) {
		this.isOccupied = isOccupied;
	}

	/**
	 * @return Returns the rownum.
	 */
	public int getRownum() {
		return rownum;
	}

	/**
	 * @param rownum
	 *            The rownum to set.
	 */
	public void setRownum(int rownum) {
		this.rownum = rownum;
	}

	/**
	 * @return Returns the hEIGHT.
	 */
	public static int getHEIGHT() {
		return HEIGHT;
	}

	/**
	 * @return Returns the wIDTH.
	 */
	public static int getWIDTH() {
		return WIDTH;
	}
}
