import java.io.BufferedReader;
import java.util.StringTokenizer;

public class TetrisWatcher extends Thread {
	private TetrisClient parent;

	private BufferedReader in;

	public TetrisWatcher(TetrisClient parent, BufferedReader in) {
		this.parent = parent;
		this.in = in;
		this.setPriority(MAX_PRIORITY);
	}

	public void run() {
		String line;
		// StringTokenizer tokens;
		try {
			while ((line = in.readLine()) != null) {
				// if (line.lastIndexOf(TetrisProtocol.DUMP) != -1) {
				if (line.startsWith(TetrisProtocol.DUMP)) {
					// get dump magnitude
					StringTokenizer tokens = new StringTokenizer(line);
					tokens.nextToken();
					parent.dumpRandom(Integer.parseInt(tokens.nextToken()));
				} // else if (line.lastIndexOf(TetrisProtocol.VICTORY) != -1)
				// {
				else if (line.startsWith(TetrisProtocol.VICTORY)) {
					parent.victory();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
