/**
 * @author jwoo
 */

import java.awt.event.WindowListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JFrame;

public class TetrisApp {
	private static final String NAME = "Tetris v1.16b";

	public static final String HOSTFILE = "src/test/resources/host";

	public static void main(String args[]) {
		JFrame.setDefaultLookAndFeelDecorated(true);

		JFrame s = new JFrame(NAME);
		SplashPanel splash = new SplashPanel(s);
		s.setContentPane(splash);
		s.pack();
		s.show();
		s.toFront();
		s.setResizable(false);
		s.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void startSingle() {
		JFrame t = new JFrame(NAME);
		TetrisPanel thePanel = new TetrisPanel();
		t.addWindowListener((WindowListener) thePanel);
		t.setContentPane(thePanel);
		t.pack();
		t.show();
		t.toFront();
		t.setResizable(false);
		t.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void startHost() {
		JFrame t = new JFrame(NAME);
		TetrisServer server = new TetrisServer();
		TetrisClient thePanel = new TetrisClient();
		t.addWindowListener((WindowListener) thePanel);
		t.setContentPane(thePanel);
		t.pack();
		t.show();
		t.toFront();
		t.setResizable(false);
		t.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void startJoin() {
		JFrame t = new JFrame(NAME);
		// TODO
		// String host = JOptionPane.showInputDialog(t, "Enter host:", "",
		// JOptionPane.QUESTION_MESSAGE);
		String host = null;
		try {
			BufferedReader inStream = new BufferedReader(new FileReader(
					HOSTFILE));
			host = inStream.readLine();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		TetrisClient thePanel = new TetrisClient(host);
		t.addWindowListener((WindowListener) thePanel);
		t.setContentPane(thePanel);
		t.pack();
		t.show();
		t.toFront();
		t.setResizable(false);
		t.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
