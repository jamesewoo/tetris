import java.applet.Applet;
import java.applet.AudioClip;
import java.net.MalformedURLException;
import java.net.URL;

public class SoundManager {
	// 1 sec bug
	public static final String DEFAULT_MOVE = "file:PickUpItem.wav";

	// 1 sec bug
	public static final String DEFAULT_ROTATE = "file:PickUpItem.wav";

	public static final String DEFAULT_LINECOMPLETE = "file:src/main/resources/sounds/FragBombs.wav";

	public static final String DEFAULT_TRIPLE = "file:src/main/resources/sounds/WarlordYes3.wav";

	public static final String DEFAULT_TETRIS = "file:src/main/resources/sounds/WarlordYes2.wav";

	public static final String DEFAULT_GAMEOVER = "file:src/main/resources/sounds/WarlordWarcry1.wav";

	private String move;

	private String rotate;

	private String lineComplete;

	private String triple;

	private String tetris;

	private String gameOver;

	public SoundManager() {
		super();
		this.gameOver = DEFAULT_GAMEOVER;
		this.lineComplete = DEFAULT_LINECOMPLETE;
		this.move = DEFAULT_MOVE;
		this.rotate = DEFAULT_ROTATE;
		this.tetris = DEFAULT_TETRIS;
		this.triple = DEFAULT_TRIPLE;
	}

	public SoundManager(String move, String rotate, String lineComplete,
			String triple, String tetris, String gameOver) {
		super();
		this.gameOver = gameOver;
		this.lineComplete = lineComplete;
		this.move = move;
		this.rotate = rotate;
		this.tetris = tetris;
		this.triple = triple;
	}

	public void playClip(String file) {
		AudioClip clip;
		try {
			clip = Applet.newAudioClip(new URL(file));
			clip.play();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

	public void preload(String file) {
		AudioClip clip;
		try {
			clip = Applet.newAudioClip(new URL(file));
			clip.play();
			clip.stop();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

	public void preloadAll() {
		preload(move);
		preload(rotate);
		preload(lineComplete);
		preload(triple);
		preload(tetris);
		preload(gameOver);
	}

	/**
	 * @return Returns the gameOver.
	 */
	public String getGameOver() {
		return gameOver;
	}

	/**
	 * @param gameOver
	 *            The gameOver to set.
	 */
	public void setGameOver(String gameOver) {
		this.gameOver = gameOver;
	}

	/**
	 * @return Returns the lineComplete.
	 */
	public String getLineComplete() {
		return lineComplete;
	}

	/**
	 * @param lineComplete
	 *            The lineComplete to set.
	 */
	public void setLineComplete(String lineComplete) {
		this.lineComplete = lineComplete;
	}

	/**
	 * @return Returns the move.
	 */
	public String getMove() {
		return move;
	}

	/**
	 * @param move
	 *            The move to set.
	 */
	public void setMove(String move) {
		this.move = move;
	}

	/**
	 * @return Returns the rotate.
	 */
	public String getRotate() {
		return rotate;
	}

	/**
	 * @param rotate
	 *            The rotate to set.
	 */
	public void setRotate(String rotate) {
		this.rotate = rotate;
	}

	/**
	 * @return Returns the tetris.
	 */
	public String getTetris() {
		return tetris;
	}

	/**
	 * @param tetris
	 *            The tetris to set.
	 */
	public void setTetris(String tetris) {
		this.tetris = tetris;
	}

	/**
	 * @return Returns the triple.
	 */
	public String getTriple() {
		return triple;
	}

	/**
	 * @param triple
	 *            The triple to set.
	 */
	public void setTriple(String triple) {
		this.triple = triple;
	}
}
